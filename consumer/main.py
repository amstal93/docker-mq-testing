import pika
import time

time.sleep(15)

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq.sys'))
channel = connection.channel()
channel.queue_declare(queue="hello")

print("Setting up a consumer", flush=True)
channel.basic_consume(queue="hello",
                      auto_ack=True,
                      on_message_callback=lambda ch, method, properties, body: print(f"Got message {body}", flush=True))

channel.start_consuming()